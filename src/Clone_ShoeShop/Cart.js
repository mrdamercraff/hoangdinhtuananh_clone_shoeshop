import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.number}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleMinusQuantity(item);
              }}
              className="btn btn-danger mr-1"
            >
              -
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleAddQuantity(item);
              }}
              className="btn btn-success ml-1"
            >
              +
            </button>
          </td>

          <td>
            <img src={item.image} alt="" style={{ width: "80px" }} />
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>PRICE</th>
            <th>QUANTITY</th>
            <th>IMAGE</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
