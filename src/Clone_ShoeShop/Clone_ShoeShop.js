import React, { Component } from "react";
import Cart from "./Cart";
import { DataShoe } from "./Clone_DataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Clone_ShoeShop extends Component {
  state = {
    shoeArr: DataShoe,
    detail: DataShoe[0],
    cart: [],
  };
  handleChangeDetail = (para) => {
    this.setState({ detail: para });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }

    this.setState({
      cart: cloneCart,
    });
  };
  handleAddQuantity = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      return;
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleMinusQuantity = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      return;
    } else {
      if (cloneCart[index].number == 1) {
        cloneCart.splice(index, 1);
      } else {
        cloneCart[index].number--;
      }
    }
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div
        className="container
      "
      >
        <Cart
          handleMinusQuantity={this.handleMinusQuantity}
          handleAddQuantity={this.handleAddQuantity}
          cart={this.state.cart}
        />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetail={this.handleChangeDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
