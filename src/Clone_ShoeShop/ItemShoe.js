import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>

            <p class="card-text">{this.props.data.description}</p>
            <h2>${this.props.data.price}</h2>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-primary mr-5"
            >
              Buy
            </button>
            <button
              type="button"
              class="btn btn-primary"
              data-toggle="modal"
              data-target="#exampleModal"
              onClick={() => {
                this.props.handleChangeDetail(this.props.data);
              }}
              className="btn btn-warning"
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
